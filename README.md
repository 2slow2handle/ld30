# README #

Platformer game made in 48 hours for 30th Ludum Dare compo

### How to run ###

```
python-pip install -r requirements.txt
python main.py
```
You also need AVBin library http://avbin.github.io/AVbin/Download.html  



This code licensed under MIT License.  
Font was created by Michel LUN, http://www.peax-webdesign.com 