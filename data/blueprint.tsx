<?xml version="1.0" encoding="UTF-8"?>
<tileset name="blueprint" tilewidth="24" tileheight="24">
 <image source="blueprint.png" width="504" height="504"/>
 <terraintypes>
  <terrain name="Новый участок" tile="-1"/>
 </terraintypes>
 <tile id="0">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile> 
 <tile id="10">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="kill" value="1"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="goto" value="default"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="kill" value="1"/>
  </properties>
 </tile>
 <tile id="64">
  <properties>
   <property name="kill" value="1"/>
  </properties>
 </tile>
 <tile id="65">
  <properties>
   <property name="goto" value="hot"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="89">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="90">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="91">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="92">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="93">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="94">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="95">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile> 
 <tile id="106">
  <properties>
   <property name="all" value="1"/>
  </properties>
 </tile>
 <tile id="107">
  <properties>
   <property name="goto" value="cold"/>
  </properties>
 </tile>
 <tile id="439">
  <properties>
   <property name="exit" value="1"/>
  </properties>
 </tile>
 <tile id="440">
  <properties>
   <property name="player_start" value="1"/>
  </properties>
 </tile>
</tileset>
