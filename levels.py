from __future__ import division, print_function, unicode_literals
import os
from cocos import tiles
from constants import *


class Level(object):
    def __init__(self, filename):
        self.filename = os.path.join(RESOURCE_FOLDER, filename)
        self.layers = []
        self.active_layer = None
        self.active_layer_name = None

    def load(self):
        #self.objects = pytmx.TiledMap(self.filename).getObjects()
        self.layers = tiles.load(self.filename).contents
        self.active_layer_name = 'default'
        self.set_active_layer('default')
        return self

    def set_active_layer(self, name):
        self.active_layer_name = name
        self.active_layer = self.layers[name]


# levels = [
#      Level('map001.tmx'),
#      Level('map002.tmx'),
#      Level('map003.tmx'),
#      Level('map004.tmx'),
#      Level('map005.tmx'),
# ]

files = []
for (dirpath, dirnames, filenames) in os.walk("data"):
    files.extend([name for name in filenames if name.startswith('map')])
files = sorted(files)

levels = []
for filename in files:
    levels.append(Level(filename))

#print(files)
#print(levels)