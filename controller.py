from __future__ import division, print_function, unicode_literals

from cocos.layer import Layer
from pyglet.window import key

__all__ = ['GameCtrl']

class GameCtrl( Layer ):

    is_event_handler = True #: enable pyglet's events

    def __init__(self, model):
        super(GameCtrl,self).__init__()

        self.used_key = False
        self.paused = True

        self.model = model
        self.elapsed = 0

    def on_key_press(self, k, m ):
        if self.paused:
            return False

        if self.used_key:
            return False

        if k == key.R:
            self.model.retry()

        # if k in (key.H, key.C, key.D):
        #     if k == key.H:
        #         global_state.level.set_active_layer('hot')
        #         print('active layer hot')
        #     elif k == key.C:
        #         global_state.level.set_active_layer('cold')
        #         print('active layer cold')
        #     elif k == key.D:
        #         global_state.level.set_active_layer('default')
        #         print('active layer default')
        #     self.used_key = True
        #     self.model.notify_switch_layer()
        #     return True
        return False

    def on_text_motion(self, motion):
        if self.paused:
            return False

        if self.used_key:
            return False

        return False

    def pause_controller( self ):
        '''removes the schedule timer and doesn't handler the keys'''
        self.paused = True
        self.unschedule( self.step )

    def resume_controller( self ):
        '''schedules  the timer and handles the keys'''
        self.paused = False
        self.schedule( self.step )

    def step( self, dt ):
        '''updates the engine'''
        self.elapsed += dt

    def draw( self ):
        '''draw the map and the block'''
        self.used_key = False
