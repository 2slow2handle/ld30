from __future__ import division, print_function, unicode_literals

from cocos.layer import ColorLayer
from cocos.director import director
from cocos.text import Label
from cocos.actions import *
from constants import *
import soundex

class GameOver( ColorLayer ):
    is_event_handler = True #: enable pyglet's events

    def __init__( self, win = False):
        super(GameOver,self).__init__( 0,0,0,200)
        #soundex.stop_music(soundex.music_player)
        #soundex.stop_music(soundex.music_player2)

        w,h = director.get_window_size()

        if win:
            soundex.play('complete.mp3')
            msg = 'THAT\'S ALL FOR NOW'
            msg2 = 'THANKS FOR PLAYING!'
        else:
            msg = 'GAME OVER'
            msg2 = ''

        label = Label(msg,
                    font_name=FONT_TITLE,
                    font_size=36,
                    anchor_y='center',
                    anchor_x='center' )
        label.position =  ( w/2.0, h/2.0 )
        self.add( label )

        label = Label(msg2,
                    font_name=FONT_TITLE,
                    font_size=24,
                    anchor_y='center',
                    anchor_x='center' )
        label.position =  ( w/2.0, h/2.0-50 )
        self.add( label )

        angle = 5
        duration = 0.05
        accel = 2
        rot = Accelerate(Rotate( angle, duration//2 ), accel)
        rot2 = Accelerate(Rotate( -angle*2, duration), accel)
        effect = rot + (rot2 + Reverse(rot2)) * 4 + Reverse(rot)
        
        label.do( Repeat( Delay(3) + effect ) )

    def on_key_press( self, k, m ):
        #if not self.hi_score and (k == key.ENTER or k == key.ESCAPE):
        #    director.pop()
        #    return True
        director.pop()

    def on_text( self, t ):
        # if not self.hi_score:
        #     return False

        if t=='\r':
            return True

        #self.name.element.text += t
