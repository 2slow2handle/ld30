from __future__ import division, print_function, unicode_literals
from actors import PlayerCollider
from sprites import PlayerSprite
from pyglet.gl import *
from cocos.layer import Layer, ColorLayer
from cocos.scene import Scene
from cocos.director import director
from cocos.actions import *
from controller import *
from model import *
import levels
import gameover
from constants import *
import soundex
from HUD import *


__all__ = ['get_newgame']

class GameView( Layer ):

    def __init__(self, model, hud, scrolling_manager ):
        super(GameView,self).__init__()

        width, height = director.get_window_size()

        aspect = width / float(height)
        self.grid_size = ( int(20 *aspect),20)
        self.duration = 8

        self.model = model
        self.hud = hud
        self.scrolling_manager = scrolling_manager

        self.player = PlayerSprite()
        self.player_layer = ScrollableLayer()
        self.player_layer.add(self.player)
        self.collider = PlayerCollider()


        self.player.do(self.collider)

        self.model.push_handlers(
                                    self.on_retry,\
                                    self.on_switch_layer,\
                                    self.on_game_over, \
                                    self.on_level_complete, \
                                    self.on_new_level, \
                                    self.on_win, \
                                    )

        self.hud.show_message( 'GET READY', self.model.start, animate=False )
        #self.model.start()

    def on_enter(self):
        super(GameView,self).on_enter()
        soundex.play_music()

    def on_exit(self):
        super(GameView,self).on_exit()
        soundex.stop_music()

    def on_level_complete( self ):
        soundex.play('complete.mp3')
        self.hud.show_message('LEVEL COMPLETE', self.model.set_next_level)
        return True

    def on_retry(self):
        soundex.play('death.mp3')
        global_state.misses += 1
        self.hud.show_message('MISSED!', lambda:global_state.scene.do(ShuffleTiles(grid=(8,6), duration=0.5) + StopGrid() + CallFunc(self.retry)))
        return True

    def retry(self):
        self.stop()
        self.do( StopGrid() )
        soundex.music_player.volume = max(soundex.music_player2.volume, soundex.music_player.volume)
        soundex.music_player2.volume = 0
        try:
            self.scrolling_manager.remove('active_layer')
            self.scrolling_manager.remove('player')
        except Exception:
            pass
        global_state.level.set_active_layer('default')
        self.scrolling_manager.add(global_state.level.active_layer, name='active_layer')
        self.scrolling_manager.add(self.player_layer, name='player')

        start = global_state.level.layers['default_meta'].find_cells(player_start=True)[0]
        r = self.player.get_rect()
        r.midbottom = start.midbottom
        self.player.position = r.center
        self.model.ctrl().resume_controller()

    def on_new_level( self ):
        if not soundex.music_player.playing:
            soundex.set_music('music.mp3', player=soundex.music_player)
            soundex.set_music('music_outside.mp3', player=soundex.music_player2)

        self.hud.show_message('LEVEL %d' % (global_state.level_idx+1))
        self.stop()
        self.do( StopGrid() )

        try:
            self.scrolling_manager.remove('active_layer')
            self.scrolling_manager.remove('player')
        except Exception:
            pass
        self.scrolling_manager.add(global_state.level.active_layer, name='active_layer')
        self.scrolling_manager.add(self.player_layer, name='player')

        start = global_state.level.layers['default_meta'].find_cells(player_start=True)[0]
        r = self.player.get_rect()
        r.midbottom = start.midbottom
        self.player.position = r.center

        return True

    def on_switch_layer(self):
        soundex.play('flip.mp3')
        if global_state.flip_3d:
            self.model.ctrl().pause()
            global_state.scene.do(FlipX3D(duration=0.4) + StopGrid() + CallFunc(self.switch_layer))
        else:
            self.switch_layer()
        return True

    def switch_layer(self):
        #self.model.ctrl().resume_controller()
        self.scrolling_manager.remove('active_layer')
        self.scrolling_manager.add(global_state.level.active_layer, name='active_layer')

    def on_game_over( self ):
        self.parent.add( gameover.GameOver(win=False), z=10 )
        return True

    def on_win( self ):
        self.parent.add( gameover.GameOver(win=True), z=10 )
        return True

    # def draw( self ):
    #     glPushMatrix()
    #     self.transform()
    #     glPopMatrix()


def get_newgame():

    soundex.stop_music(soundex.music_player)
    soundex.stop_music(soundex.music_player2)
    soundex.music_player.volume = max(soundex.music_player2.volume, soundex.music_player.volume)
    soundex.music_player2.volume = 0
    soundex.current_music = None

    scene = Scene()

    # model
    model = GameModel()

    global_state.reset()
    global_state.model = model
    global_state.scene = scene

    # controller
    ctrl = GameCtrl(model)

    scrolling_manager = ScrollingManager()
    scrolling_manager.scale = 2

    # view
    hud = HUD()
    view = GameView( model, hud, scrolling_manager )

    # set controller in model
    model.set_controller(ctrl)

    # add controller
    scene.add( ctrl, z=1, name="controller" )

    # add view
    scene.add( hud, z=10, name="hud" )
    scene.add( BackgroundLayer('bg_game.jpg'), z=0, name="background" )
    scene.add( view, z=3, name="view" )

    fadeable = ColorLayer(0,0,0,255)
    fadeable.add(scrolling_manager, z=4)
    fadeable.do(FadeOut(1.5))

    #scene.add( scrolling_manager, z=3, name="scrolling_manager")
    scene.add(fadeable, z=2)
    return scene
