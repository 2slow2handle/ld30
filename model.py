from __future__ import division, print_function, unicode_literals

import weakref
import pyglet

from constants import *
from global_state import global_state
import levels


__all__ = ['GameModel']


class GameModel(pyglet.event.EventDispatcher):

    def __init__(self):
        super(GameModel,self).__init__()

        self.init()
        #global_state.reset()
        global_state.level = levels.levels[0]

    def start(self):
        #print('GameModel.start')
        self.set_next_level()

    def set_controller(self, ctrl):
        self.ctrl = weakref.ref( ctrl )

    def check_win(self):
        #print('idx, total: ', global_state.level_idx, len(levels.levels)-1)
        self.ctrl().pause_controller()
        if global_state.level_idx >= len(levels.levels)-1:
            self.dispatch_event("on_win")
        else:
            self.dispatch_event("on_level_complete")
        return False

    def notify_switch_layer(self):
        #print('notify_switch_layer')
        self.dispatch_event("on_switch_layer")

    def retry(self):
        self.ctrl().pause_controller()
        #print('retry')
        self.dispatch_event("on_retry")

    def init( self ):
        pass

    def set_next_level( self ):
        #print('GameModel.set_next_level')
        self.ctrl().resume_controller()
        #self.check_win()

        if global_state.level_idx is None:
            global_state.level_idx = 0
        else:
            global_state.level_idx += 1

        #print('global_state.level_idx ', global_state.level_idx)

        l = levels.levels[global_state.level_idx]

        self.init()
        global_state.level = l.load()

        self.dispatch_event("on_new_level")


GameModel.register_event_type('on_level_complete')
GameModel.register_event_type('on_new_level')
GameModel.register_event_type('on_retry')
GameModel.register_event_type('on_switch_layer')
GameModel.register_event_type('on_game_over')
GameModel.register_event_type('on_win')
