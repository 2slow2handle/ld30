from __future__ import division, print_function, unicode_literals
import soundex
from sprites import PlayerSprite
from cocos.actions import Action
from cocos.tiles import RectMapCollider
from pyglet.window import key
from global_state import global_state


class PlayerCollider(Action, RectMapCollider):
    on_ground = True
    MOVE_SPEED = 60
    JUMP_SPEED = 300
    GRAVITY = -1500
    DEFAULT_GRAVITY = -1500
    DEFAULT_MOVE_SPEED = 80

    def __init__(self):
        self.goto = None
        self.exit = None
        self.kill = None
        super(PlayerCollider, self).__init__()

    def start(self):
        # initial velocity
        self.target.velocity = (0, 0)

    def step(self, dt):
        if global_state.model.ctrl().paused:
            return False

        from global_state import keyboard
        dx, dy = self.target.velocity

        dx = (keyboard[key.RIGHT] - keyboard[key.LEFT]) * self.MOVE_SPEED * dt

        if dx > 0:
            direction = 'r'
        elif dx < 0:
            direction = 'l'
        else:
            direction = None

        #print(dx, direction, self.target.state, self.target.direction)
        if direction and self.target.state != 'jump' and (self.target.state != 'walk' or self.target.direction <> direction):
            self.target.animate('walk', direction)
        elif not direction and self.target.state == 'walk':
            self.target.animate('stay', self.target.direction)
        elif self.target.state == 'jump' and self.on_ground:
            self.target.animate('stay', self.target.direction)

        dy = dy + self.GRAVITY * dt

        if self.on_ground  and keyboard[key.SPACE]:
            self.target.animate('jump', self.target.direction)
            soundex.play('jump2.mp3')
            dy = self.JUMP_SPEED

        last = self.target.get_rect()
        new = last.copy()
        new.x += dx
        new.y += dy * dt

        last.gravity = PlayerCollider.GRAVITY
        last.move_speed = PlayerCollider.MOVE_SPEED

        dx, dy = self.target.velocity = self.collide_map(global_state.level.active_layer, last, new, dy, dx)
        self.on_ground = bool(new.y == last.y)

        self.target.position = new.center

        if keyboard[key.UP]:
            if self.goto:
                if (global_state.level.active_layer_name == 'default' and self.goto in ['hot', 'cold']) or \
                   (global_state.level.active_layer_name in ['hot', 'cold'] and self.goto == 'default' ):
                    soundex.music_player.volume, soundex.music_player2.volume = soundex.music_player2.volume, soundex.music_player.volume
                global_state.level.set_active_layer(self.goto)
                global_state.model.notify_switch_layer()
            elif self.exit:
                global_state.model.check_win()

        if self.kill:
            global_state.model.retry()
        #self.scrolling_manager.set_focus(*new.center)

    def do_collision(self, cell, last, new, dy, dx):
        '''Collide a Rect moving from "last" to "new" with the given map
        RectCell "cell". The "dx" and "dy" values may indicate the velocity
        of the moving rect.

        The RectCell must have the boolean properties "top", "left",
        "bottom" and "right" for those sides which the rect should collide.

        If there is no collision then nothing is done.

        If there is a collision:

        1. The "new" rect's position will be modified to its closest position
           to the side of the cell that the collision is on, and
        2. If the "dx" and "dy" values are passed in the methods
           collide_<side> will be called to indicate that the rect has
           collided with the cell on the rect's side indicated. The value
           passed to the collide method will be a *modified* distance based
           on the position of the rect after collision (according to step
           #1).

        Mutates the new rect to conform with the map.

        Returns the (possibly modified) (dx, dy)
        '''

        if global_state.model.ctrl().paused:
            return False

        g = cell.get

        gravity = float(g('gravity')) if g('gravity') else None
        move_speed = float(g('move_speed')) if g('move_speed') else None

        if gravity and last.gravity != gravity:
            PlayerCollider.GRAVITY = gravity

        if move_speed and last.move_speed != move_speed:
            PlayerCollider.MOVE_SPEED = move_speed

        if (g('top') or g('all')) and last.bottom >= cell.top-20 and new.bottom < cell.top and last.right > cell.left and last.left < cell.right: # -20 to avoid tonelling :D
            dy = last.y - new.y
            new.bottom = cell.top
            if dy: self.collide_bottom(dy)
        if (g('left') or g('all')) and last.right <= cell.left and new.right > cell.left and last.bottom < cell.top:
            dx = last.x - new.x
            new.right = cell.left
            if dx: self.collide_right(dx)
        if (g('right') or g('all')) and last.left >= cell.right and new.left < cell.right and last.bottom < cell.top:
            dx = last.x - new.x
            new.left = cell.right
            if dx: self.collide_left(dx)
        if (g('bottom') or g('all')) and last.top <= cell.bottom and new.top > cell.bottom:# and last.right > cell.left: # ???
            dy = last.y - new.y
            new.top = cell.bottom
            if dy: self.collide_top(dy)

        if g('slope_up'):
            floor = cell.bottom + last.right-cell.left
            if new.bottom < floor and new.left < cell.right - 6:
                new.bottom = floor
                dy = last.y - new.y
            if dy: self.collide_bottom(dy)

        if g('slope_down'):
            floor = cell.bottom + cell.right - last.left
            if new.bottom < floor and new.right > cell.left + 6:
                new.bottom = floor
                dy = last.y - new.y
            if dy: self.collide_bottom(dy)

        self.goto = g('goto')
        self.exit = g('exit')

        if g('kill') and new.bottom <= cell.top-10:
            self.kill = True
        else:
            self.kill = False

        return dx, dy


# class Player(object):
#
#     def __init__(self):
#         self.sprite = PlayerSprite()
#         self.sprite.do(PlayerCollider())