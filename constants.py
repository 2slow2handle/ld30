from __future__ import division, print_function, unicode_literals

MUSIC = True
SOUND = True
RESOURCE_FOLDER = 'data'
FONT_TITLE = 'PWChalk'
FLIP3D = True