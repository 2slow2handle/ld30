from __future__ import division, print_function, unicode_literals

import os
from cocos.layer import *
from cocos.text import *
from cocos.actions import *
from pyglet.gl import *
from global_state import global_state
from constants import *


class BackgroundLayer(Layer):
    def __init__( self, path_name ):
        super(BackgroundLayer, self).__init__()
        #self.image = pyglet.resource.image('bg_menu.jpg') # raises exception @ tiles.py, line 558
        self.image = image.load(os.path.join(RESOURCE_FOLDER, path_name))

    def draw( self ):
        glPushMatrix()
        self.transform()
        self.image.blit(0,0)
        glPopMatrix()


class ScoreLayer( Layer ): 
    def __init__(self):
        w,h = director.get_window_size()
        super( ScoreLayer, self).__init__()

        # transparent layer
        #self.add( ColorLayer(32,32,32,32, width=w, height=48),z=-1 )

        self.position = (0,h-48)

        self.misses = Label('', font_size=28,
                font_name=FONT_TITLE,
                color=(255,255,255,255),
                anchor_x='right',
                anchor_y='bottom')
        self.misses.position=(w-92,0)
        self.add( self.misses)

        self.lvl = Label('', font_size=28,
                font_name=FONT_TITLE,
                color=(255,255,255,255),
                anchor_x='left',
                anchor_y='bottom')

        self.lvl.position=(92,0)
        self.add( self.lvl)

    def draw(self):
        super( ScoreLayer, self).draw()
        self.lvl.element.text = 'LEVEL: %d' % (global_state.level_idx+1 if global_state.level_idx else 1)
        self.misses.element.text = 'MISSES: %d' % global_state.misses


class MessageLayer( Layer ):
    def show_message( self, msg, callback=None, delay=1, animate=True):

        w,h = director.get_window_size()

        self.msg = Label( msg,
            font_size=52,
            font_name=FONT_TITLE,
            anchor_y='bottom',
            anchor_x='center' )
        self.msg.position=(w//2.0, h)

        self.add( self.msg )

        if animate:
            actions = Accelerate(MoveBy( (0,-h/2.0), duration=0.5)) + \
                        Delay(delay) +  \
                        Accelerate(MoveBy( (0,-h/2.0), duration=0.5)) + \
                        Hide()
        else:
            actions = Accelerate(MoveBy( (0,-h/2.0), duration=1)) + Delay(delay) + Hide()

        if callback:
            actions += CallFunc( callback )

        self.msg.do( actions )


class HUD( Layer ):
    def __init__( self ):
        super( HUD, self).__init__()
        self.add( ScoreLayer() )
        self.add( MessageLayer(), name='msg' )

    def show_message( self, msg, callback = None, delay=1, animate=True ):
        self.get('msg').show_message( msg, callback, delay, animate )
