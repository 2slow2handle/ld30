from __future__ import division, print_function, unicode_literals
from pyglet.window import key


__all__ = ['global_state', 'keyboard']


class GlobalState(object):
    def __init__( self ):
        self.reset()
        self.flip_3d = True

    def reset( self ):
        self.misses = 0
        self.level = None
        self.level_idx = None
        self.model = None
        self.scene = None

global_state = GlobalState()
keyboard = key.KeyStateHandler()