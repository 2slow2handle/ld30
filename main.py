from __future__ import division, print_function, unicode_literals

from cocos.director import director
from cocos.layer import *
from cocos.scene import Scene
from cocos.scenes.transitions import *
from cocos.actions import *
from cocos.sprite import *
from cocos.menu import *
from cocos.text import *
from constants import *

import pyglet
from pyglet import gl, font
from pyglet.window import key

from HUD import BackgroundLayer
import soundex
import os
from global_state import global_state


class OptionsMenu( Menu ):
    def __init__(self):
        super( OptionsMenu, self).__init__('OPTIONS')
        self.select_sound = soundex.load('select.mp3')

        self.font_title['font_name'] = FONT_TITLE
        self.font_title['font_size'] = 62
        self.font_title['color'] = (255,255,255,255)

        self.font_item['font_name'] = FONT_TITLE,
        self.font_item['color'] = (255,255,255,255)
        self.font_item['font_size'] = 48
        self.font_item_selected['font_name'] = FONT_TITLE
        self.font_item_selected['color'] = (255,16,32,255)
        self.font_item_selected['font_size'] = 48

        # example: menus can be vertical aligned and horizontal aligned
        self.menu_anchor_y = CENTER
        self.menu_anchor_x = CENTER

        items = []

        self.volumes = ['MUTE','10','20','30','40','50','60','70','80','90','100']

        items.append( ToggleMenuItem('3D FLIP: ', self.on_flip_3d, global_state.flip_3d))

        items.append( MultipleMenuItem(
                        'SFX VOL: ',
                        self.on_sfx_volume,
                        self.volumes,
                        int(soundex.sound_vol * 10) )
                    )
        items.append( MultipleMenuItem(
                        'MUSIC VOL: ',
                        self.on_music_volume,
                        self.volumes,
                        int(soundex.music_player.volume * 10) )
                    )
        items.append( ToggleMenuItem('SHOW FPS: ', self.on_show_fps, director.show_FPS) )
        items.append( MenuItem('FULLSCREEN', self.on_fullscreen) )
        items.append( MenuItem('BACK', self.on_quit) )
        # self.create_menu( items, shake(), shake_back() )
        self.create_menu( items )

    def on_fullscreen( self ):
        director.window.set_fullscreen( not director.window.fullscreen )

    def on_quit( self ):
        self.parent.switch_to( 0 )

    def on_show_fps( self, value ):
        director.show_FPS = value

    def on_sfx_volume( self, idx ):
        vol = idx / 10.0
        soundex.sound_volume( vol )

    def on_music_volume( self, idx ):
        vol = idx / 10.0
        soundex.music_volume( vol )

    def on_flip_3d( self, value ):
        global_state.flip_3d = value


class MainMenu( Menu ):

    def __init__(self):
        super( MainMenu, self).__init__('BLUEPRINT DUDE')

        self.select_sound = soundex.load('select.mp3')

        self.font_title['font_name'] = FONT_TITLE
        self.font_title['font_size'] = 62
        self.font_title['color'] = (255,255,255,255)

        self.font_item['font_name'] = FONT_TITLE,
        self.font_item['color'] = (255,255,255,255)
        self.font_item['font_size'] = 48
        self.font_item_selected['font_name'] = FONT_TITLE
        self.font_item_selected['color'] = (255,16,32,255)
        self.font_item_selected['font_size'] = 48

        self.menu_anchor_y = CENTER
        self.menu_anchor_x = CENTER

        items = []

        items.append( MenuItem('NEW GAME', self.on_new_game) )
        items.append( MenuItem('OPTIONS', self.on_options) )
        # items.append( MenuItem('Scores', self.on_scores) )
        items.append( MenuItem('QUIT', self.on_quit) )

        # self.create_menu( items, shake(), shake_back() )
        self.create_menu( items )

    def on_new_game(self):
        #import levels

        import view
        director.push( FadeTRTransition(
            view.get_newgame(), 1.5 ) )

    def on_options( self ):
        self.parent.switch_to(1)

    def on_scores( self ):
        self.parent.switch_to(2)

    def on_quit(self):
        pyglet.app.exit()


class TextLayer(Layer):

    def __init__(self):
        super(TextLayer, self).__init__()
        w, h = director.get_window_size()
        label = Label('LEFT/RIGHT ARROW - MOVE AROUND\nSPACE - JUMP\nUP ARROW - USE SWITCHER\nR - RESTART LEVEL',
            font_name=FONT_TITLE,
            color=(255,255,255,255),
            font_size=18,
            width=500,
            anchor_x='left',
            anchor_y='center',
            multiline=True)

        label.position = 30, 80
        self.add(label)

        label = Label('LD 30 COMPO / HATE-MARINA\n2SLOW2HANDLE@GMAIL.COM',
            font_name=FONT_TITLE,
            color=(255,255,255,255),
            font_size=12,
            width=300,
            anchor_x='left',
            anchor_y='center',
            multiline=True)

        label.position = w-320, 70
        self.add(label)

if __name__ == "__main__":

    from global_state import keyboard

    pyglet.resource.path.append(RESOURCE_FOLDER)
    pyglet.resource.reindex()
    font.add_directory(RESOURCE_FOLDER)

    director.init( resizable=False, width=1344, height=768 )

    scene = Scene()
    scene.add(TextLayer(), z=2)

    scene.add( MultiplexLayer(
                    MainMenu(),
                    OptionsMenu(),
                    #ScoresLayer(),
                    ),
                z=1 )

    scene.add(BackgroundLayer('bg_game.jpg'), z=0)


    director.window.push_handlers(keyboard)
    director.run( scene )
